import sqlite3

def init_db():
    conn = sqlite3.connect('game_scores.db')
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS scores (
            id INTEGER PRIMARY KEY,
            username TEXT NOT NULL,
            score INTEGER NOT NULL
        )
    ''')
    conn.commit()
    conn.close()


def add_score(username, score):
    conn = sqlite3.connect('game_scores.db')
    cursor = conn.cursor()
    cursor.execute('INSERT INTO scores (username, score) VALUES (?, ?)', (username, score))
    conn.commit()
    conn.close()


def get_high_scores(limit=5):
    conn = sqlite3.connect('game_scores.db')
    cursor = conn.cursor()
    cursor.execute('SELECT username, score FROM scores ORDER BY score DESC LIMIT ?', (limit,))
    scores = cursor.fetchall()
    conn.close()
    return scores

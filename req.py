import pygame

pygame.init()
screen = pygame.display.set_mode((640, 480))
black = (0,0,0)
small_text = pygame.font.Font("freesansbold.ttf",20)

bg_img = pygame.image.load('media/run-bg.png').convert_alpha()
bg_img = pygame.transform.scale(bg_img,(640,480))


class InputBox:
    def __init__(self, x, y, w, h, text=''):
        self.rect = pygame.Rect(x, y, w, h)
        self.color = black
        self.text = text
        self.txt_surface = small_text.render(text, True, self.color)

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                print(self.text)
                self.text = ''
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            else:
                self.text += event.unicode
            # re-render the text
            self.txt_surface = small_text.render(self.text, True, self.color)

    def draw(self, screen):
        screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))
        pygame.draw.rect(screen, self.color, self.rect, 2)


def main():
    box = InputBox((640 - 140)/2, (480 - 32)/2, 140, 32)
    done = False

    while not done:
        screen.blit(bg_img,(0,0))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            box.handle_event(event)

        box.draw(screen)
        pygame.display.flip()

if __name__ == '__main__':
    main()
    pygame.quit()
